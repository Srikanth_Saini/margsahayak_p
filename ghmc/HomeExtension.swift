//
//  HomeExtension.swift
//  ghmc
//
//  Created by Sraoss on 03/12/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import Foundation
import  UIKit

extension HomeViewController{
    func getCommonOTpMethod(Complition: @escaping ()->(Void)){
        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
        self.delegate?.notInteract()
        self.Indicator.startAnimating()
                  let data : Data = "MobileNumber=\(MobileNumber)&appName=Citizen Sentinel".data(using:String.Encoding.ascii, allowLossyConversion: false)!
                  var request = URLRequest(url: URL.init(string: "https://edistrict.ncog.gov.in/RNB_mob_data/CommanOtpMethod")!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                  
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    
                        guard error == nil, let data = data else{
                            DispatchQueue.main.async {
                                self.delegate?.Interact()
                            self.Indicator.stopAnimating()
                                self.popupAlert(title: "", message: error?.localizedDescription, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            }
                           return
                        }
                        
                        do{
                            
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            self.OTP = (dataResponse["otp"] as? String)!
                            print(self.OTP)
                           Complition()
                            
                        }catch{
                            DispatchQueue.main.async {
                                self.delegate?.Interact()
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            }
                            
                        }
                    
                  })
                  task.resume()
                  
       
        
       
        
    }
    
    func GetVerifyOTPMethod( lat:String, long:String, Complition: @escaping ()->(Void)){
        
        
        print(self.OTP)
        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
        DispatchQueue.main.async {
            self.delegate?.notInteract()
             self.Indicator.startAnimating()
        }
               
                  let data : Data = "MobileNumber=\(MobileNumber)&appName=Citizen Sentinel&otpMessage=\(self.OTP)&latitude=\(lat)&longitude=\(long)".data(using:String.Encoding.ascii, allowLossyConversion: false)!
                   
                  var request = URLRequest(url: URL.init(string: "https://edistrict.ncog.gov.in/RNB_mob_data/VerifayOTPMethod")!)
                  request.httpMethod = "POST"
                  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                  request.httpBody = data
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.delegate?.Interact()
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           return
                        }
                        
                        do{
                            self.delegate?.Interact()
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            print(dataResponse)
                            
                            if dataResponse["status"] as? String == "true"{
                                let AvailabelDic = dataResponse["data"] as! [[String:Any]]
                                self.RoadNo = AvailabelDic[0]["uniqe_code"] as? String ?? ""
                                self.Roadname = AvailabelDic[0]["name"] as? String ?? ""
                                
                            }
                            
                            if dataResponse["status"] as? String == "false"{
                                self.errorHandlingApi()
                            }
                            Complition()
                            
                        }catch{
                            self.delegate?.Interact()
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            
                        }
                    }
                  })
                  task.resume()
                  
       
        
    }
    
    
    func errorHandlingApi(){

        let MobileNumber = UserDefaults.standard.value(forKey: UserdefaultKeys.MobileNumber.rawValue) as! String
        let parameters:[String:Any] = ["phoneNo":MobileNumber,"location": [ 23.234223, 72.245433 ],"message":" Please Enter Valid OTP."]

          do {
                  let data = try JSONSerialization.data(withJSONObject: parameters, options: [])

                  var request = URLRequest(url: URL.init(string: "http://roadgrievancealpha.herokuapp.com/api/android/error")!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()

                           return
                        }

                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            print(dataResponse)

                        }catch{
                            self.Indicator.stopAnimating()


                        }
                    }
                  })
                  task.resume()

        }catch{
            self.Indicator.stopAnimating()

        }

    }
    
    
    
    func AddComplaint(Indicator:UIActivityIndicatorView,GrievamceType:String,date:NSDate, lat:Double, long:Double){
        guard self.ImageName != nil else{
            self.popupAlert(title: "", message: "Please try again, Uploading failed", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        guard self.Roadname != nil,self.RoadNo != nil else{
            self.popupAlert(title: "", message: "Select grievance Type", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        Indicator.startAnimating()
        self.delegate?.notInteract()
        let parameters:[String:Any] = ["url":self.ImageName!,"road_code":self.RoadNo!,"name":self.Roadname!, "griev_type":GrievamceType, "location": [ lat, long ], "description":"Testing of Application. Not real Complaint"]
              // print(parameters)
                 do {
                         let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                                
                         var request = URLRequest(url: URL.init(string: "http://roadgrievancealpha.herokuapp.com/api/android/postNewComplaint")!)
                         request.httpMethod = "POST"
                         request.httpBody = data
                         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                         let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
                    request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
                         let task = URLSession.shared.dataTask(with: request, completionHandler: {
                             data,response,error in
                           DispatchQueue.main.async {
                               guard error == nil, let data = data else{
                                self.delegate?.Interact()
                                   Indicator.stopAnimating()
                                   self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                  return
                               }
                               
                               do{
                                   Indicator.stopAnimating()
                                self.delegate?.Interact()
                                   let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                                   let success = dataResponse["success"] as? Bool
                                if success ?? false{
                                    self.Roadname = String()
                                    self.RoadNo = String()
                                    self.popupAlert(title: "", message: "Complaint added successfully", Style: .alert, actionTitles: ["OK"], actions: [{
                                        action in
                                        self.GetOnlineComplaints()
                                        }])
                                    CoredataHandler.shared.DeleteRecord(date)
                                    self.OfflineComplaints = CoredataHandler.shared.GetSavedDataFromCoredata() as! [Complaint]
                                    if self.OfflineComplaints.count == 0{
                                        self.TableHeaderView.isHidden = true
                                        self.TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
                                        self.view.layoutIfNeeded()
                                        self.ComplaintsTableView.reloadData()
                                    }else{
                                        self.TableHeaderView.isHidden = false
                                        self.TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 190)
                                        self.view.layoutIfNeeded()
                                    }
                                    self.OfflineCollectionView.reloadData()
                                    if self.OfflineComplaints.count == 0 && self.OnlineComplaints.count == 0  {
                                        self.ComplaintsTableView.isHidden = true
                                    }
                                }else{
                                    self.delegate?.Interact()
                                    Indicator.stopAnimating()
                                    self.popupAlert(title: "", message: dataResponse["data"] as? String, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                }
                                   
                               }catch{
                                self.delegate?.Interact()
                                   Indicator.stopAnimating()
                                   self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                   
                               }
                           }
                         })
                         task.resume()
                         
               }catch{
                   Indicator.stopAnimating()
                     
               }
        
    }
    
    
    func UPloadImage(imageName:String){
        self.delegate?.notInteract()
           var r  = URLRequest(url: URL(string: AddImage_Url)!)
           r.httpMethod = "POST"
           let boundary = "Boundary-\(UUID().uuidString)"
           r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
           r.httpBody = createBody(boundary: boundary, data: self.ImageData!, mimeType: "image/jpg", filename: imageName)
           let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
           r.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
           
           let task = URLSession.shared.dataTask(with: r, completionHandler: {
               (data,response,error) in
               guard error == nil, let data = data else{
                   DispatchQueue.main.async {
                     self.delegate?.Interact()
                    self.popupAlert(title: "", message: error?.localizedDescription, Style: .alert, actionTitles: ["OK"], actions: [nil])
                   }
                   return
               }
               
               do{
                   
                   let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                   let success = dataResponse["success"] as? Bool
                   print(dataResponse)
                   DispatchQueue.main.async {
                       if success == true{
                           self.ImageName = dataResponse["data"] as? String
                           
                       }else{
                           self.delegate?.Interact()
                           self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           
                       }
                   }
                   
                   
               }catch{
                   DispatchQueue.main.async {
                       self.delegate?.Interact()
                       self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                   }
               }
               
               
               
           })
           task.resume()
       }
       
       func createBody(boundary: String,
                       data: Data,
                       mimeType: String,
                       filename: String) -> Data {
           let body = NSMutableData()
           
           let boundaryPrefix = "--\(boundary)\r\n"
           
           //        for (key, value) in parameters {
           //            body.appendString(boundaryPrefix)
           //            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
           //            body.appendString("\(value)\r\n")
           //        }
           
           body.appendString(boundaryPrefix)
           body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
           body.appendString("Content-Type: \(mimeType)\r\n\r\n")
           body.append(data)
           body.appendString("\r\n")
           body.appendString("--".appending(boundary.appending("--")))
           
           return body as Data
       }
}
