//
//  RegistrationViewController.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var EmailTf: UITextField!
    @IBOutlet weak var FullNameTf: UITextField!
    @IBOutlet weak var WhiteCardView: UIView!
    @IBOutlet weak var WhiteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.WhiteView.frame = CGRect.init(x: 0, y: self.view.frame.height/2, width: self.view.frame.width, height: self.view.frame.height/2)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.WhiteCardView.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
        self.EmailTf.addBottomBorder()
        EmailTf.tintColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        EmailTf.textColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        EmailTf.leftView = Singletons.shared.textfieldImage(UIImage(named: "30_mail")!)
        EmailTf.leftViewMode = .always
        
        self.FullNameTf.addBottomBorder()
        FullNameTf.tintColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        FullNameTf.textColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        FullNameTf.leftView = Singletons.shared.textfieldImage(UIImage(named: "30_edit")!)
        FullNameTf.leftViewMode = .always
        
        
    }
    
    //MARK:- SAVE ACTION
    
    @IBAction func SaveAction(_ sender: Any) {
        
        guard FullNameTf.text?.count != 0 else{
            self.popupAlert(title: "", message: "Please enter your full name.", Style: .alert
                , actionTitles: ["OK"], actions: [nil])
            return
        }
        if EmailTf.text?.count ?? 0 > 0{
            guard EmailTf.text!.isValidEmail() else{
                self.popupAlert(title: "", message: "Please enter valid email.", Style: .alert
                    , actionTitles: ["OK"], actions: [nil])
                return
            }
        }
        
        
        guard NetworkState.isConnected() else{
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        Indicator.startAnimating()
        var m = "name=\(FullNameTf.text!)&email=\(EmailTf.text ?? "NA")"
        if EmailTf.text == ""{
            m = "name=\(FullNameTf.text!)&email=\("NA")"
        }
        let data : Data = m.data(using:String.Encoding.ascii, allowLossyConversion: false)!
        var request = URLRequest(url: URL.init(string: Signup_url)!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
        request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
        request.httpBody = data
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data,response,error in
            DispatchQueue.main.async {
                guard error == nil, let data = data else{
                    self.Indicator.stopAnimating()
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                    return
                }
                
                do{
                    self.Indicator.stopAnimating()
                    let dataResponse = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! Dictionary<String, Any>
                    let success = dataResponse["success"] as? Bool
                    
                    if success == true{
                        UserDefaults.standard.setValue(true, forKey: UserdefaultKeys.IsSignUPCompleted.rawValue)
                        let Token = dataResponse["data"] as? String
                        UserDefaults.standard.setValue(Token, forKey: UserdefaultKeys.tokenId.rawValue)
                        Navigation().setrootViewControllerTo("HomeID")
                        
                    }else{
                        
                        self.popupAlert(title: "", message:SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                        
                    }
                }catch{
                    self.Indicator.stopAnimating()
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                    
                }
            }
        })
        task.resume()
        
    }
    
}
