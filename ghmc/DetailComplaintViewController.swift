//
//  DetailComplaintViewController.swift
//  ghmc
//
//  Created by udaykumar on 27/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit
import Kingfisher

class DetailComplaintViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var ComplaintTableView: UITableView!
    var complaint = recoverComplaints()
    var imageView = UIImageView()
    var DismissButton=UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        ComplaintTableView.delegate = self
        ComplaintTableView.dataSource = self
        ComplaintTableView.contentInset = UIEdgeInsets(top: 300, left: 0, bottom: 0, right: 0)
        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300)
        let imgUrl = (complaint.url)!.replace(target: " ", withString:"%20")
        imageView.kf.setImage(with: URL(string: imgUrl))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        view.addSubview(imageView)
        
        DismissButton.frame = CGRect.init(x: 10, y: 30, width: 40, height: 40)
        DismissButton.setImage(UIImage.init(named: "Dismiss"), for: .normal)
        DismissButton.addTarget(self, action: #selector(DismissAction), for: .touchUpInside)
        self.view.addSubview(DismissButton)

    }
    
    @objc func DismissAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 215.0
        }
        if indexPath.row == 2{
            return 150
            
        }
        return 150
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
             let cell:DetailTableViewCell1 = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell1", for: indexPath) as! DetailTableViewCell1
            cell.GrievanceLabel.text = complaint.griev_type
            cell.ComplaintIdLabel.text = complaint._id
            cell.StatusLabel.text = complaint.complaint_status
            cell.RoadNameLabel.text = complaint.road_code
            cell.SubmitDateLabel.text = complaint.complaint_upload_date
            cell.SubmitTimeLabel.text = complaint.complaint_upload_time
             return cell
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DtailTableViewCell2", for: indexPath) as! DtailTableViewCell2
            cell.OfficerNameLabel.text = "Officer Name: \(complaint.officer_name ?? "N/A")"
            cell.EstimatedTimeLabel.text = "Estimated Complition Date: \(complaint.estimated_time ?? "NO EST Available")"
            if complaint.comments?.count ?? 0 > 0{
            cell.CommentsLabel.text = "Comment: \(complaint.comments?[0] ?? "No comments")"
            }
            cell.CommentsLabel.text = "Comment: No comments"
            
                return cell
        }
        if indexPath.row == 2{
            let cell:DtailTableViewCell3 = tableView.dequeueReusableCell(withIdentifier: "DtailTableViewCell3", for: indexPath) as! DtailTableViewCell3
            
            cell.DescriptionLabel.text = complaint.description
                return cell
        }
        
        return UITableViewCell()

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let y = 300 - (scrollView.contentOffset.y + 300)
    let height = min(max(y, 60), 400)
    imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
    }
    
    
    

  

}
