//
//  ViewController.swift
//  ghmc
//
//  Created by Uday on 19/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit
import Alamofire

class VerifyViewController: UIViewController {

    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var MobileNumberTF: UITextField!
    @IBOutlet weak var WhiecardView: UIView!
    @IBOutlet weak var whiteView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.whiteView.frame = CGRect.init(x: 0, y: self.view.frame.height/2, width: self.view.frame.width, height: self.view.frame.height/2)
        Indicator.stopAnimating()
        Indicator.hidesWhenStopped = true
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.WhiecardView.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
        self.MobileNumberTF.addBottomBorder()
        MobileNumberTF.tintColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        MobileNumberTF.textColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        MobileNumberTF.leftView = Singletons.shared.textfieldImage(UIImage(named: "callIcon")!)
        MobileNumberTF.leftViewMode = .always
    }
    
    @IBAction func VerifyMobileNumber(_ sender:AnyObject){
        guard MobileNumberTF.text?.count == 10 else {
            self.popupAlert(title: "", message: "Please enter valid mobile number", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        guard NetworkState.isConnected() else{
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        Indicator.startAnimating()
        let parameters:[String:Any] = ["phoneNo":MobileNumberTF.text!]
          do {
                  let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                         
                  var request = URLRequest(url: URL.init(string: Login_Url)!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("application/json", forHTTPHeaderField: "Accept")
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           return
                        }
                        
                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            let success = dataResponse["success"] as? Bool
                            if success == true{
                                
                                self.performSegue(withIdentifier: "MobileToOtp", sender: self)
                            }else{
                                
                                self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                
                            }
                        }catch{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            
                        }
                    }
                  })
                  task.resume()
                  
        }catch{
                  
        }
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MobileToOtp"{
            let vc = segue.destination as! OTPViewController
            vc.mobileNumber = self.MobileNumberTF.text!
        }
    }
    
    


}

