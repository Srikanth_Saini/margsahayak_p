//
//  HomeViewController.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit
import Foundation
import Kingfisher

protocol CellUploadInteractions {
    func notInteract()
    func Interact()
}

class HomeViewController: UIViewController {
    var delegate:CellUploadInteractions?
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var OfflineCollectionView: UICollectionView!
    @IBOutlet weak var TableHeaderView: UIView!
    @IBOutlet weak var ComplaintsTableView: UITableView!
    @IBOutlet weak var BottonNavigationBar: UINavigationBar!
    var OfflineComplaints = [Complaint]()
    var OnlineComplaints = [recoverComplaints]()
    var OTP = ""
    var RoadNo:String?
    var Roadname:String?
    var ImageName:String?
    var ImageData:Data?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now()+2.0, execute: {
            if !(NetworkState.isConnected()){
                self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                
            }
        })
        BottonNavigationBar.shadowImage = UIImage()
        OfflineCollectionView.delegate = self
        OfflineCollectionView.dataSource = self
        ComplaintsTableView.delegate = self
        ComplaintsTableView.dataSource = self
        Indicator.stopAnimating()
        Indicator.hidesWhenStopped = true
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        OfflineComplaints = CoredataHandler.shared.GetSavedDataFromCoredata() as! [Complaint]
        GetOnlineComplaints()
        if OfflineComplaints.count == 0{
            TableHeaderView.isHidden = true
            TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
            self.view.layoutIfNeeded()
        }else{
            TableHeaderView.isHidden = false
            TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 190)
            self.view.layoutIfNeeded()
        }
        ComplaintsTableView.reloadData()
        OfflineCollectionView.reloadData()
        
        self.ComplaintsTableView.isHidden = false
        if OfflineComplaints.count == 0 && OnlineComplaints.count == 0  {
            self.ComplaintsTableView.isHidden = true
        }
    }
    
    
}

//MARK:- TableView Methods

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OnlineComplaints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TCell", for: indexPath) as! ComplaintsTableViewCell
        let item = OnlineComplaints[indexPath.row]
        cell.grevianceLabel.text = item.griev_type
        cell.TimeLabel.text = "Reported on:\(item.complaint_upload_date ?? "")"
        cell.StausLabel.text = "Status:\(item.complaint_status ?? "")"
        let imgUrl = (item.url)!.replace(target: " ", withString:"%20")
        cell.ComplaintImageView.kf.setImage(with: URL(string: imgUrl))
        cell.ComplaintImageView.kf.indicatorType = .activity
//        cell.ComplaintImageView.downloaded(from: item.url!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: 250, height: 60))
        label.text = "Your Complaints"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .lightGray
        view.addSubview(label)
        let button = UIButton.init(frame: CGRect.init(x: self.view.frame.width - 100, y: 5, width: 100, height: 50))
//        button.setTitle("Filter", for: .normal)
        button.setImage(UIImage.init(named: "filterBlack"), for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Filter    ", for: .normal)
        button.titleLabel?.font =  UIFont.boldSystemFont(ofSize: 18)
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(FilterAction), for: .touchUpInside)
        view.addSubview(button)
        view.backgroundColor = #colorLiteral(red: 0.9289386334, green: 0.9289386334, blue: 0.9289386334, alpha: 1)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.OnlineComplaints.count > 0{
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "HomeDetail") as! DetailComplaintViewController
            vc.complaint = OnlineComplaints[indexPath.row]
            //vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetail") as! DetailComplaintViewController
            vc.complaint = OnlineComplaints[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    //Filter Button Action
    @objc func FilterAction(){
        
        self.performSegue(withIdentifier: "FilterShow", sender: self)
        
    }
    
}



//MARK:- CollectionView Methods

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return OfflineComplaints.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CCell", for: indexPath) as! OfflineCollectionViewCell
        let item = OfflineComplaints[indexPath.row]
        cell.OImageView.image = UIImage.init(data: item.imagedata!)
        cell.OLabel.text = "\(item.grievance ?? "")"
        
        cell.Deleteaction = {
            CoredataHandler.shared.DeleteRecord(item.date! as NSDate)
            self.OfflineComplaints = CoredataHandler.shared.GetSavedDataFromCoredata() as! [Complaint]
            if self.OfflineComplaints.count == 0{
                self.TableHeaderView.isHidden = true
                self.TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
                self.view.layoutIfNeeded()
                self.ComplaintsTableView.reloadData()
            }else{
                self.TableHeaderView.isHidden = false
                self.TableHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 190)
                self.view.layoutIfNeeded()
            }
            self.OfflineCollectionView.reloadData()
            if self.OfflineComplaints.count == 0 && self.OnlineComplaints.count == 0  {
                self.ComplaintsTableView.isHidden = true
            }
        }
        cell.Uploadaction = {
            self.delegate = cell.self
            guard NetworkState.isConnected()else{
                self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                return
            }
             DispatchQueue.main.async {
                guard let imageData = cell.OImageView.image!.jpegData(compressionQuality: 0.4) else {
                           return
                       }
                       self.ImageData = imageData
                       let dataformatter = DateFormatter()
                       dataformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                       let Datestring = dataformatter.string(from: Date())
                       self.UPloadImage(imageName: "\(Datestring).jpeg")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.getCommonOTpMethod(Complition: {
                        self.GetVerifyOTPMethod(lat: "\(item.latitude)", long:"\(item.longitude)", Complition: {
                            self.AddComplaint(Indicator: cell.Indicator, GrievamceType: item.grievance ?? "N/A",date:item.date! as NSDate, lat: item.latitude,long: item.longitude)
                        })
                    })
                })
                
                   }
           
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 150)
    }
    
}


//MARK:- PARSING ONLINE COMPLAINTS

extension HomeViewController{
    
    func GetOnlineComplaints(){
        guard NetworkState.isConnected() else {
//            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        Indicator.startAnimating()
        var request = URLRequest(url: URL.init(string: RecoverComplaints_Url)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
        request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            (data,response,error) in
            guard error == nil, let data = data else{
                DispatchQueue.main.async {
                    self.Indicator.stopAnimating()
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
                return
            }
            
            do{
                let complaints = try JSONDecoder().decode([recoverComplaints].self, from: data)
                print(complaints)
                self.OnlineComplaints = complaints
                
                
                DispatchQueue.main.async {
                    self.Indicator.stopAnimating()
                    if self.OnlineComplaints.count > 0{
                        self.ComplaintsTableView.isHidden = false
                    }
                    self.ComplaintsTableView.reloadData()
                }
                
            }catch{
                DispatchQueue.main.async {
                    self.Indicator.stopAnimating()
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
            }
            
            
            
        })
        task.resume()
        
    }
    
}
