//
//  PageViewController.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController]()
    let pageControl = UIPageControl()
    let skipButton : UIButton = {
        let skipButton = UIButton()
        skipButton.translatesAutoresizingMaskIntoConstraints = false
        skipButton.setTitle("SKIP", for: .normal)
        skipButton.tintColor = .yellow
        skipButton.setTitleColor(.green, for: .normal)
        
        return skipButton
    }()
    
    let NextButton : UIButton = {
           let skipButton = UIButton()
           skipButton.translatesAutoresizingMaskIntoConstraints = false
           skipButton.setTitle("NEXT", for: .normal)
           skipButton.setTitleColor(.white, for: .normal)
           skipButton.backgroundColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
           return skipButton
       }()
    
    var DismissButton = UIButton()
    
    var pagenumber = 0
    var fromHome = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .lightGray
        self.dataSource = self
        self.delegate = self
        let initialPage = 0
        let page1 = Page1ViewController()
        let page2 = Page2ViewController()
        let page3 = Page3ViewController()
        let page4 = Page4ViewController()
        
        // add the individual viewControllers to the pageViewController
        self.pages.append(page1)
        self.pages.append(page2)
        self.pages.append(page3)
        self.pages.append(page4)
        setViewControllers([pages[initialPage]], direction: .forward, animated: true, completion: nil)
        
        // pageControl
        self.pageControl.frame = CGRect()
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.numberOfPages = self.pages.count
        self.pageControl.currentPage = initialPage
        
        self.view.addSubview(self.pageControl)
        
        self.pageControl.translatesAutoresizingMaskIntoConstraints = false
        self.pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -5).isActive = true
        self.pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -20).isActive = true
        self.pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        self.view.addSubview(skipButton)
        skipButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        skipButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        skipButton.addTarget(self, action: #selector(skipButtonAction), for: .touchUpInside)
        
        self.view.addSubview(NextButton)
        NextButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        NextButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30).isActive = true
        NextButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
       // NextButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        NextButton.addTarget(self, action: #selector(NextButtonAction), for: .touchUpInside)
        
        self.DismissButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(DismissButton)
        DismissButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25).isActive = true
        DismissButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        DismissButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        DismissButton.widthAnchor.constraint(equalToConstant: 140).isActive = true
//        DismissButton.setImage(UIImage.init(named: "Dismiss"), for: .normal)
        DismissButton.setTitle("Dismiss", for: .normal)
        DismissButton.setTitleColor(.black, for: .normal)
        DismissButton.addTarget(self, action: #selector(DismissButtonAction), for: .touchUpInside)
        if fromHome{
            skipButton.isHidden = true
            NextButton.isHidden = true
            DismissButton.isHidden = false
        }else{
            DismissButton.isHidden = true
        }
        
    }
    
    @objc func NextButtonAction(){
        
        guard fromHome == false else{return}
        
        if NextButton.titleLabel?.text == "FINISH"{
            
            UserDefaults.standard.setValue(true, forKey: "IsOnBoardingCompleted")
            Navigation().setrootViewControllerTo("SignUP")
        }
        pagenumber = pagenumber+1
        if pagenumber < 3{
            
        self.setViewControllers([self.pages[pagenumber]],
        direction: .forward,
        animated: true,
        completion: nil)
            if pagenumber == 2{
                skipButton.isHidden = true
                NextButton.setTitle("FINISH", for: .normal)
            }else{
            skipButton.isHidden = false
            NextButton.setTitle("NEXT", for: .normal)
            }
            
        }else{
            skipButton.isHidden = true
            NextButton.setTitle("FINISH", for: .normal)
        }
        
    }
    
    
    @objc func skipButtonAction(){
        UserDefaults.standard.setValue(true, forKey: "IsOnBoardingCompleted")
        Navigation().setrootViewControllerTo("SignUP")
        
    }
    
    @objc func DismissButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.pages.firstIndex(of: viewController) {
            if viewControllerIndex == 0 {
                // wrap to last page in array
                //return self.pages.last
                return nil
            } else {
                // go to previous page in array
                
                return self.pages[viewControllerIndex - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.pages.firstIndex(of: viewController) {
            if viewControllerIndex < self.pages.count - 1 {
                // go to next page in array
                if fromHome == false{
                skipButton.isHidden = false
                }
                NextButton.setTitle("NEXT", for: .normal)
                return self.pages[viewControllerIndex + 1]
                
            } else {
                // wrap to first page in array
                //skipButton.setTitle("Get Started", for: .normal)
               // return self.pages.first
                skipButton.isHidden = true
                NextButton.setTitle("FINISH", for: .normal)
                return nil
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        // set the pageControl.currentPage to the index of the current viewController in pages
        if let viewControllers = pageViewController.viewControllers {
            if let viewControllerIndex = self.pages.firstIndex(of: viewControllers[0]) {
                self.pageControl.currentPage = viewControllerIndex
            }
        }
    }
    
    
    
}

