//
//  Page3ViewController.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class Page3ViewController: UIViewController {

    var imageview : UIImageView = {
        let img = UIImageView()
        img.image = UIImage.init(named: "3")
        return img
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.lightGray
        imageview.frame = self.view.frame
        self.view.addSubview(imageview)
    }

}
