//
//  Page4ViewController.swift
//  ghmc
//
//  Created by mac on 12/5/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class Page4ViewController: UIViewController {

      var imageview : UIImageView = {
          let img = UIImageView()
          img.image = UIImage.init(named: "4")
          return img
      }()

      override func viewDidLoad() {
          super.viewDidLoad()
          self.view.backgroundColor = UIColor.lightGray
          imageview.frame = self.view.frame
          self.view.addSubview(imageview)
      }

  

}
