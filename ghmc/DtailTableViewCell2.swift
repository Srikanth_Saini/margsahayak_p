//
//  DtailTableViewCell2.swift
//  ghmc
//
//  Created by Sraoss on 28/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class DtailTableViewCell2: UITableViewCell {
    @IBOutlet weak var mainBackView: UIView!
    @IBOutlet weak var OfficerNameLabel:UILabel!
    @IBOutlet weak var EstimatedTimeLabel:UILabel!
    @IBOutlet weak var CommentsLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainBackView.CreateShadow(cornerRadius: 0.0, shadowColor: .lightGray)
        OfficerNameLabel.adjustsFontSizeToFitWidth = true
        EstimatedTimeLabel.adjustsFontSizeToFitWidth = true
        CommentsLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
