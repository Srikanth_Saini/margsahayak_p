//
//  FilterMenuViewController.swift
//  ghmc
//
//  Created by Uday on 26/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit
import DropDown



class FilterMenuViewController: UIViewController {
    var delegate: FilterTableReloadDelegate?
    @IBOutlet weak var ToTf: UITextField!
    @IBOutlet weak var FromTf: UITextField!
    @IBOutlet weak var Grievance: UILabel!
    let GrievancedropDown = DropDown()
    @IBOutlet weak var StatusLabel: UILabel!
    let StatusdropDown = DropDown()
    var OnlineComplaints = [recoverComplaints]()
    var Grievances = [grievancename]()
    var GNames = [String]()
    let datePicker = UIDatePicker()
    var filteredArray = [recoverComplaints]()
    @IBOutlet weak var BackView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetFilterComplaintsForFilter()
        showDateToPicker()
        showDateFromPicker()
        //Adding Tap Gesture to SelectGrievenceLabel to identify userTap
        let tap = UITapGestureRecognizer(target: self, action: #selector(selectGrivenveAction))
        Grievance.addGestureRecognizer(tap)
        Grievance.isUserInteractionEnabled = true
        GrievancedropDown.anchorView = Grievance
        GrievancedropDown.center = self.view.center
        GrievancedropDown.dataSource = ["DAMAGED BRIDGE( પુલ ને નુકશાન )", "DAMAGED BRIDGE PARAPET( પુલ ની પેરાપેટને નુકશાન )", "BREACH ON A ROAD ( રસ્તા પર ભંગ )", "DAMAGED RAILING( રેલિંગ ને નુકશાન )", "DAMAGE STRUCTURES( મકાન ને નુકશાન )", "POT HOLES( પોટહોલ )", "FALLEN TREE( ઝાડ પડેલ છે )", "DEGREDED ROADS(નષ્ટ રસ્તાઓ)", "Road/Bridge Overtopped( રોડ/પુલ પર પાણી ભરાઈ જાય છે )"]
        GrievancedropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.Grievance.text = item
        }
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(selectStausAction))
        StatusLabel.addGestureRecognizer(tap1)
        StatusLabel.isUserInteractionEnabled = true
        StatusdropDown.anchorView = StatusLabel
        StatusdropDown.center = self.view.center
        StatusdropDown.dataSource = ["ALL", "Approved", "Pending","Rejected","In Progress","Completed"]
        StatusdropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.StatusLabel.text = item
//            guard self.StatusLabel.text != "ALL"else{return}
//            self.filteredArray = FilterComplaints.filter { $0.complaint_status == self.StatusLabel.text }
        }
        
        DropDown.startListeningToKeyboard()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        BackView.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
    }
    
    
    
    
    
    @IBAction func ApplyAction(_ sender: Any) {
        filteredArray = FilterComplaints
        if self.StatusLabel.text != "Apply Status" {
            if self.StatusLabel.text != "ALL"{
            filteredArray = filteredArray.filter { $0.complaint_status == self.StatusLabel.text }
            }
        }
        
        if self.Grievance.text != "Apply Grievance"{
            filteredArray = filteredArray.filter { $0.griev_type == self.Grievance.text }
        }
        
        if self.FromTf.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd:MM:yyyy"
            filteredArray = filteredArray.filter{ dateFormatter.date(from:$0.complaint_upload_date!)! >= dateFormatter.date(from:self.FromTf.text!)! }
        }
        
        if self.FromTf.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd:MM:yyyy"
            filteredArray = filteredArray.filter{ dateFormatter.date(from:$0.complaint_upload_date!)! <= dateFormatter.date(from:self.ToTf.text!)! }
        }
       
         
        
        FilterComplaints = filteredArray
        
        delegate?.ReloadFilterTableView()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func CancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func selectGrivenveAction(){
//        getGrievance()
        GrievancedropDown.show()
        
    }
    
    @objc func selectStausAction(){
        StatusdropDown.show()
    }
    
    
    // GET GRIEvance
    
    func getGrievance(){
        guard NetworkState.isConnected() else {
               self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
               return
           }
        var request = URLRequest(url: URL.init(string: grienvances_Url)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
        request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            (data,response,error) in
            guard error == nil, let data = data else{
                 DispatchQueue.main.async {
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
                return
            }
            
            do{
                let Grievance = try JSONDecoder().decode(grienvanceModel.self, from: data)
                self.Grievances = Grievance.data
                var names = [String]()
                for g in self.Grievances{
                    names.append(g.name!)
                    
                }
                self.GNames = names
                print(self.GNames)
                DispatchQueue.main.async {
                    self.GrievancedropDown.dataSource = self.GNames
                    self.GrievancedropDown.reloadAllComponents()
                    self.GrievancedropDown.show()
                }
                
            }catch{
                DispatchQueue.main.async {
                    
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
            }
        })
        task.resume()
        
        
    }
    
    func GetFilterComplaintsForFilter(){
        guard NetworkState.isConnected() else {
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
       
        var request = URLRequest(url: URL.init(string: RecoverComplaints_Url)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let token = UserDefaults.standard.value(forKey: UserdefaultKeys.tokenId.rawValue) as? String
        request.addValue("token \(token ?? "")", forHTTPHeaderField: "auth")
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            (data,response,error) in
            guard error == nil, let data = data else{
                 DispatchQueue.main.async {
                    
                    self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                }
                return
            }
            
            do{
                let complaints = try JSONDecoder().decode([recoverComplaints].self, from: data)
                FilterComplaints = complaints
               
               

            }catch{
                
            }
            
            
            
        })
        task.resume()
        
    }
    
    
    func showDateFromPicker(){
        //Formate Date
        datePicker.datePickerMode = .date

       //ToolBar
       let toolbar = UIToolbar();
       toolbar.sizeToFit()
       let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
      let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

     toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
       

      
        
        FromTf.inputAccessoryView = toolbar
        FromTf.inputView = datePicker
        
        

     }

      @objc func donedatePicker(){

       let formatter = DateFormatter()
       formatter.dateFormat = "dd:MM:yyyy"
       FromTf.text = formatter.string(from: datePicker.date)
        
       self.view.endEditing(true)
     }

     @objc func cancelDatePicker(){
        self.view.endEditing(true)
      }
    
    
    func showDateToPicker(){
       //Formate Date
       datePicker.datePickerMode = .date

      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedateToPicker));
       let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDateToPicker));

    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
      ToTf.inputAccessoryView = toolbar
       ToTf.inputView = datePicker
       
       

    }

     @objc func donedateToPicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "dd:MM:yyyy"
      ToTf.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDateToPicker(){
       self.view.endEditing(true)
     }
    
    
    

}
