//
//  OTPViewController.swift
//  ghmc
//
//  Created by Uday on 20/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var TimerLabel: UILabel!
    @IBOutlet weak var OTPTextField: UITextField!
    @IBOutlet weak var PhonenumberLabel: UILabel!
    @IBOutlet weak var WhiteCardView: UIView!
    @IBOutlet weak var WhiteView: UIView!
    var mobileNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.WhiteView.frame = CGRect.init(x: 0, y: self.view.frame.height/2, width: self.view.frame.width, height: self.view.frame.height/2)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.WhiteCardView.CreateShadow(cornerRadius: 5.0, shadowColor: .gray)
        self.OTPTextField.addBottomBorder()
        OTPTextField.tintColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        OTPTextField.textColor = #colorLiteral(red: 0.1501098275, green: 0.4749142528, blue: 0.7598940134, alpha: 1)
        PhonenumberLabel.text = mobileNumber
        
    }
    
    @IBAction func Dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK:- Resend OTP
    @IBAction func ResendOTPAction(_ sender: Any) {
        guard NetworkState.isConnected() else{
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        Indicator.startAnimating()
        let parameters:[String:Any] = ["phoneNo":PhonenumberLabel.text!]
          do {
                  let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                         
                  var request = URLRequest(url: URL.init(string: Login_Url)!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("application/json", forHTTPHeaderField: "Accept")
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           return
                        }
                        
                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            let success = dataResponse["success"] as? Bool
                            if success == true{
                                self.popupAlert(title: "", message: OtpSuccessfulMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            }else{
                                
                                self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                
                            }
                        }catch{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            
                        }
                    }
                  })
                  task.resume()
                  
        }catch{
            Indicator.startAnimating()
            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
        }
        
    }
    
    
    //MARK:- Verify OTP
    
    @IBAction func VerifyAction(_ sender: Any) {
        
        guard OTPTextField.text?.count == 4 else {
            self.popupAlert(title: "", message: "Please enter valid OTP", Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        guard NetworkState.isConnected() else{
            self.popupAlert(title: "", message: NoInternetMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
            return
        }
        
        Indicator.startAnimating()
        let parameters:[String:Any] = ["phoneNo":PhonenumberLabel.text!,"otpNo":OTPTextField.text!]
          do {
                  let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                         
                  var request = URLRequest(url: URL.init(string: Otp_Url)!)
                  request.httpMethod = "POST"
                  request.httpBody = data
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("application/json", forHTTPHeaderField: "Accept")
                  let task = URLSession.shared.dataTask(with: request, completionHandler: {
                      data,response,error in
                    DispatchQueue.main.async {
                        guard error == nil, let data = data else{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                           return
                        }
                        
                        do{
                            self.Indicator.stopAnimating()
                            let dataResponse = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
                            let success = dataResponse["success"] as? Bool
                            let dataMessage = dataResponse["data"] as? String
                            if success == true{
                                UserDefaults.standard.set(self.PhonenumberLabel.text, forKey: UserdefaultKeys.MobileNumber.rawValue)
                                UserDefaults.standard.set(dataMessage, forKey: UserdefaultKeys.tokenId.rawValue)
                                self.performSegue(withIdentifier: "OtpToSignup", sender: self)
                            }else{
                                
                                self.popupAlert(title: "", message:dataMessage ?? SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                                
                            }
                        }catch{
                            self.Indicator.stopAnimating()
                            self.popupAlert(title: "", message: SomethingWrongMessage, Style: .alert, actionTitles: ["OK"], actions: [nil])
                            
                        }
                    }
                  })
                  task.resume()
                  
        }catch{
                  
        }
        
        
    }
    

}
