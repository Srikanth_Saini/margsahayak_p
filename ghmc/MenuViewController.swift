//
//  MenuViewController.swift
//  ghmc
//
//  Created by Uday on 22/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet var Swipegesture: UISwipeGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
    
    }
    
    @IBAction func SwipeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Logout(_ sender: Any) {
        UserDefaults.standard.setValue(true, forKey: "IsOnBoardingCompleted")
        UserDefaults.standard.setValue(false, forKey: UserdefaultKeys.IsSignUPCompleted.rawValue)
        CoredataHandler.shared.resetAllRecords("Complaint")
        Navigation().setrootViewControllerTo("SignUP")
    }

    @IBAction func HelpACtion(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: "PageVC") as! PageViewController
            vc.fromHome = true
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let vc = storyboard?.instantiateViewController(withIdentifier: "PageVC") as! PageViewController
            vc.fromHome = true
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func ContactUsAction(_ sender: Any) {
    }
}
