//
//  AppConstants.swift
//  ghmc
//
//  Created by udaykumar on 23/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import Foundation

enum UserdefaultKeys: String {
  case MobileNumber  = "MobileNumber"
  case IsSignUPCompleted = "IsSignUPCompleted"
  case tokenId = "tokenId"
  
}

let Base_Url = "http://roadgrievancealpha.herokuapp.com/api/android/"
let Login_Url = Base_Url+"otp"
let Otp_Url = Base_Url+"otp-verify"
let Signup_url = Base_Url+"signup"
let RecoverComplaints_Url = Base_Url+"recoverComplaints"
let grienvances_Url = Base_Url+"grienvances"
let AddImage_Url = Base_Url+"image"

let SomethingWrongMessage = "Something went wrong\nPlease try again"
let NoInternetMessage = "No internet connection\nPlease connect to network."
let OtpSuccessfulMessage = "OTP sent successfully to your mobile number"


