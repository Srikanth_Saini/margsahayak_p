//
//  File.swift
//  ghmc
//
//  Created by Uday on 22/11/19.
//  Copyright © 2019 Uday. All rights reserved.
//

import Foundation
import Alamofire

class NetworkState{
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
