//
//  LocationManager.swift
//  ghmc
//
//  Created by udaykumar on 22/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import Foundation
import MapKit

class LocationManager{
   
    static let shared = LocationManager()
    
    func getLoactionCoordinates() -> CLLocation{
        let locManager = CLLocationManager()
        locManager.requestWhenInUseAuthorization()
        locManager.requestAlwaysAuthorization()

        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return CLLocation()
            }
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
            
            return currentLocation
        }
        
        return CLLocation()
        
    }
}
