//
//  OfflineCollectionViewCell.swift
//  ghmc
//
//  Created by udaykumar on 23/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class OfflineCollectionViewCell: UICollectionViewCell,CellUploadInteractions {
    
    
    
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var OImageView:UIImageView!
    @IBOutlet weak var OLabel:UILabel!
    var Home = HomeViewController()
    var Deleteaction:(()->(Void))?
    var Uploadaction:(()->(Void))?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.OImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.OImageView.clipsToBounds = true
        self.OImageView.contentMode = .scaleAspectFill
        Home.delegate = self
       
    }
    
    
    @IBAction func DeleteAction(_ sender:AnyObject){
        if let btnAct = Deleteaction{
            btnAct()
        }
        
    }
    
    @IBAction func UploadAction(_ sender:AnyObject){
        if let btnAct = Uploadaction{
            btnAct()
        }
           
    }
    
    func notInteract() {
        self.isUserInteractionEnabled = false
        self.Indicator.startAnimating()
        print("gfhfh")
    }
    
    func Interact() {
        self.isUserInteractionEnabled = true
        self.Indicator.stopAnimating()
    }
    
    
    
}
