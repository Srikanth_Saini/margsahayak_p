//
//  recoverModel.swift
//  ghmc
//
//  Created by Sraoss on 25/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import Foundation

class recoverComplaints:Codable{
    var _id: String?
    var url: String?
    var location = [Double]()
    var complaint_status: String?
    var comments: [String]?
    var road_code: String?
    var name: String?
    var griev_type: String?
    var description:String?
    var estimated_time: String?
    var officer_id: String?
    var officer_email: String?
    var officer_name: String?
    var completed_img_url: String?
    var complaint_upload_date:String?
    var complaint_upload_time:String?
}



