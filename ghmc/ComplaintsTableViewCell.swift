//
//  ComplaintsTableViewCell.swift
//  ghmc
//
//  Created by Sraoss on 25/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class ComplaintsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var StausLabel:UILabel!
    @IBOutlet weak var TimeLabel:UILabel!
    @IBOutlet weak var grevianceLabel:UILabel!
    @IBOutlet weak var ComplaintImageView:UIImageView!
    @IBOutlet weak var Overlay:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.3)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
