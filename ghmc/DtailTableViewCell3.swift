//
//  DtailTableViewCell3.swift
//  ghmc
//
//  Created by Sraoss on 28/11/19.
//  Copyright © 2019 Sraoss. All rights reserved.
//

import UIKit

class DtailTableViewCell3: UITableViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var DescriptionLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        backView.CreateShadow(cornerRadius: 0.0, shadowColor: .lightGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
